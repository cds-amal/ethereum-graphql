# Prerequisite

- Install [Geth >= 1.9.0](https://geth.ethereum.org/install-and-build/Installing-Geth)
- Run Geth: `geth --nodiscover --graphql`
- Install and run [EthQL](https://github.com/ConsenSys/ethql#quickstart)