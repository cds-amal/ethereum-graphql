Based on Pantheon's [test blocks](https://github.com/PegaSysEng/pantheon/tree/master/testutil/src/main/resources) and
[test suites](https://github.com/PegaSysEng/pantheon/tree/master/ethereum/graphql/src/test/resources/tech/pegasys/pantheon/ethereum/graphql)

# Prerequisites

- Geth
- Node.js

# Run

Set up your node environment by running `npm install`.

Open a terminal and run `./init-and-run.sh`. Wait until it prints "GraphQL endpoint opened".  It takes only a few seconds.

Open another terminal and run `node main.test.js eth/*.json`. It'll run all the tests.