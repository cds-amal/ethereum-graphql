#!/bin/sh

rm -rf testdata
geth --datadir testdata init testGenesis.json
geth --datadir testdata --gcmode=archive import testBlockchain.blocks
geth --datadir testdata --nodiscover --graphql

