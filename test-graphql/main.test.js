const argv = require('yargs').argv
const tap = require('tap')
const fs = require('fs')
const { request } = require('graphql-request')


const tests_or_files = argv._

tests_or_files.forEach(tname => {
    if (tname.endsWith('.json')) {
        tname = tname.slice(0, - ".json".length)
    }
    tap.test(tname, async t => {
        const expected = JSON.parse(fs.readFileSync(`${tname}.json`, "utf8"))
        const query = fs.readFileSync(`${tname}.graphql`, "utf8")
        request('http://localhost:8547/graphql', query).then((data, error) => {
            result = {}
            if (data) {
                result.data = data
            }
            if (error) {
                result.error = error
            }
            tap.deepEqual(result, expected)
            t.end()
        })
    })
})